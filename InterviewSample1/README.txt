Objective:

1. Your task is to complete ATMMachine missing pieces of code. All Unit tests should pass.
2. Find all TODO tags in the project. IT will help you in completing the project.
3. All unit tests from project should pass.
4. Make sure you read all comments in the project.
5. If you need any help or guidance, please contact me at suhaib.qaiser@binarystar.biz.