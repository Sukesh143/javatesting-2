package biz.binarystar.testing.InterviewSample1;

import java.util.Date;

/**
* This is an ATMTransaction class that is used to define attributes for Transactions.
* When transaction happens we need to add an entry on our ledger for that transaction.
* 
* 
* @author  BinaryStar
* @version 1.0
* @since   2016-08-15
*/
public class ATMTransaction {
	
	/**
	 * Date at which transaction took place. You can change data type if you want.
	 */
	private Date transactionDate;
	
	/**
	 * Account number for the account. You can change data type if you want.
	 */
	private String accountNumber;
	
	/**
	 * Transaction type of that particular transaction.
	 */
	private TransactionType transactionType;
	
	/**
	 * Balance on account before transaction took place.
	 */
	private double openingBalance;
	
	/**
	 * Balance on account after transaction took place.
	 */
	private double closingBalance;
	
	/**
	 * Transaction Type for the ATMTransaction
	 * @author BinaryStar
	 *
	 */
	public enum TransactionType {Debit, Credit}


	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}


	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}


	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	/**
	 * @return the transactionType
	 */
	public TransactionType getTransactionType() {
		return transactionType;
	}


	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}


	/**
	 * @return the openingBalance
	 */
	public double getOpeningBalance() {
		return openingBalance;
	}


	/**
	 * @param openingBalance the openingBalance to set
	 */
	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}


	/**
	 * @return the closingBalance
	 */
	public double getClosingBalance() {
		return closingBalance;
	}


	/**
	 * @param closingBalance the closingBalance to set
	 */
	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	};
}
